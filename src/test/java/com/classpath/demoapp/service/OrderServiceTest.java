package com.classpath.demoapp.service;

import com.classpath.demoapp.model.Order;
import com.classpath.demoapp.model.OrderLineItem;
import com.classpath.demoapp.repository.ItemRepository;
import com.classpath.demoapp.repository.OrderRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class OrderServiceTest {

    @Mock
    private ItemRepository itemRepository;

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderServiceImpl orderService;

    @Test
    public void testSaveOrder(){
        Order order = new Order();
        order.setOrderId(12);
        order.setPrice(12000);
        order.setQty(23);
        OrderLineItem lineItem = new OrderLineItem();
        lineItem.setPrice(1000);
        lineItem.setName("IPad");
        lineItem.setId(11);
        order.getLineItems().add(lineItem);

        //set the expectation on the mock object
        when(orderRepository.save(order)).thenReturn(order);

        //invocation
        Order savedOrder = this.orderService.placeOrder(order);

        //assert
        assertNotNull(savedOrder);
        assertEquals(savedOrder.getOrderId(), 12);

        //verification
        verify(orderRepository, times(1)).save(any());

    }
}