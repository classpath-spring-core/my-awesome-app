package com.classpath.demoapp.controller;

import com.classpath.demoapp.model.Item;
import com.classpath.demoapp.service.OrderService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import java.util.Arrays;
import java.util.HashSet;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(OrderController.class)
public class OrderRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private OrderService orderService;

    @Before
    public void setupMockMvc(){
        mockMvc = MockMvcBuilders
                    .webAppContextSetup(context)
                    .build();
    }

    @Test
    @WithMockUser(username = "kiran", password = "dummy", roles = "ADMIN")
    public void testFetchAllOrders() throws Exception {
        //expectation
        when(orderService.fetchAll()).thenReturn(new HashSet<>(Arrays.asList(new Item())));
        //execution
        mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/items")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }
}