package com.classpath.demoapp.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class ItemTest {

    private static Item item = new Item();

    @Before
    public void setUp(){
        item.setName("MacBook-Pro");
        item.setPrice(200000);
        item.setId(12);
  }
    @Test
    public void testConstructor(){
        //initialization
        //verification
        assertEquals(item.getName(), "MacBook-Pro");
        assertEquals(item.getId(), 12);
        assertEquals(item.getPrice(), 200000,0);
    }

    @Test
    public void testPositiveEqualsAndHashCode(){
        Item anotherItem = new Item();
        anotherItem.setName("MacBook-Air");
        anotherItem.setPrice(2000);
        anotherItem.setId(12);

        assertTrue(item.equals(anotherItem));
        assertTrue(item != anotherItem);
    }

    @Test
    public void testToStringDef(){
        Item anotherItem = new Item();
        anotherItem.setName("MacBook-Pro");
        anotherItem.setPrice(200000);
        anotherItem.setId(12);
        assertTrue(item.toString().equals(anotherItem.toString()));
        assertEquals("Item(id=12, name=MacBook-Pro, price=200000.0)", item.toString());
    }
}