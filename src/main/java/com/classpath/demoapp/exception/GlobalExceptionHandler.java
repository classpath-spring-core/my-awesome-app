package com.classpath.demoapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ItemNotFoundException.class)
    public final ResponseEntity<Object> handleResourceNotFoundException (Exception exception, WebRequest request){
        List<String> errorList = new ArrayList<>();
        errorList.add(exception.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(exception.getMessage(), errorList);
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RuntimeException.class)
    public final ResponseEntity<String> handleRuntimeException(Exception ex){
        System.out.println(ex.getMessage());
        ex.printStackTrace();
        return new ResponseEntity<>("Please Try after some time", HttpStatus.BAD_REQUEST);
    }
}