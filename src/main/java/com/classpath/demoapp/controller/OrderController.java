package com.classpath.demoapp.controller;

import com.classpath.demoapp.exception.ItemNotFoundException;
import com.classpath.demoapp.model.Item;
import com.classpath.demoapp.model.Order;
import com.classpath.demoapp.model.OrderLineItem;
import com.classpath.demoapp.repository.ItemRepository;
import com.classpath.demoapp.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/")
@AllArgsConstructor
@Api(value = "Order API")
public class OrderController {

    private OrderService orderService;

    private RestTemplate restTemplate;

    private ItemRepository itemRepository;

    private Environment environment;

  // private SecurityContextHolder securityContextHolder;


    @GetMapping("/")
    @ApiOperation(value = "Get all the items using the controller")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "All Items "),
            @ApiResponse(code = 404, message = "Invalid URL")
    })
    public Set<Item> fetchAllItems(){
//        String[] beans = applicationContext.getBeanDefinitionNames();
//        for(String bean: beans) {
//            System.out.printf(" Bean Name :: %s %n", bean );
//        }
        return this.orderService.fetchAll();
    }

    @PostMapping("/")
    public Item saveItem(@RequestBody @Valid Item item){
        return this.orderService.saveItem(item);
    }

    @GetMapping("/{itemId}")
    public Item fetchItemById(@PathVariable("itemId") long itemId) throws ItemNotFoundException {
        return this.orderService.fetchItemById(itemId);
    }

    @PutMapping("/{itemId}")
    public Item updateItem(@PathVariable("itemId") long itemId, @RequestBody Item item) throws ItemNotFoundException {
        return this.orderService.updateItem(itemId, item);
    }

    @DeleteMapping("/{itemId}")
    public void deleteItem(@PathVariable("itemId") long itemId){
        this.orderService.deleteItemById(itemId);
    }

    @GetMapping("/beans")
    public String makeRestAPICall() {
        ResponseEntity<String> response = restTemplate.getForEntity("https://my-json-server.typicode.com/prashdeep/courseflix/courses/", String.class);
        HttpHeaders responseHeaders = response.getHeaders();
        while (responseHeaders.entrySet().iterator().hasNext()) {
            System.out.println(responseHeaders.entrySet().iterator().next());
        }
        /*Authentication authentication = this.securityContextHolder.getContext().getAuthentication();
        if (authentication.isAuthenticated()) {
            return ((UserDetails) authentication.getPrincipal()).getUsername();
        }*/
        return "Anonymous User";
    }


    @GetMapping("/noofitems")
    public int noOfItems(){
        return this.itemRepository.noOfItems();
    }

    @PostMapping("/orders")
    public Order createOrder(@RequestBody Order order){
        return this.orderService.placeOrder(order);
    }

    @GetMapping("/orders")
    public  Set<Order> fetchAllOrders(){
        return this.orderService.fetchAllOrders();
    }
}