package com.classpath.demoapp.repository;

import com.classpath.demoapp.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
    //DSL
    @Query("select item from Item item where item.name = ?1")
    //SQL: select * from table name where col =
    List<Item> findItemByName(String name);

    @Query(value = "select count(*) from items", nativeQuery = true)
    int noOfItems();
}