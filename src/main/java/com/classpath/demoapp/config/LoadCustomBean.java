package com.classpath.demoapp.config;

import org.springframework.boot.autoconfigure.condition.*;
import org.springframework.boot.system.JavaVersion;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnJava(JavaVersion.ELEVEN)
public class LoadCustomBean {

    @Bean
    @ConditionalOnProperty(prefix = "app", value = "check", havingValue = "true", matchIfMissing = true)
    public CustomBean customBean(){
        return new CustomBean();
    }

    @Bean
    @ConditionalOnJava(JavaVersion.EIGHT) //AND
    @ConditionalOnMissingBean(CustomBean.class)
    public CustomSerializer customSerializer(){
        return new CustomSerializer();
    }

    @Bean
    @Conditional(CustomPlatformCondition.class)
    public OsBean osBean(){
        return new OsBean();
    }
}