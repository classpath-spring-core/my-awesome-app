package com.classpath.demoapp.config;

import com.classpath.demoapp.repository.ItemRepository;
import com.classpath.demoapp.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Configuration
public class ApplicationConfiguration {

    @Autowired
    private Environment environment;

    @Autowired ItemRepository itemRepository;

    @Autowired
    OrderRepository orderRepository;
    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate =  new RestTemplate();
        HttpClientRestInterceptor interceptor = new HttpClientRestInterceptor();
        List<ClientHttpRequestInterceptor> interceptors = restTemplate.getInterceptors();
        interceptors.add( interceptor);
        return  restTemplate;
    }

    /*
    @Bean
    public OrderService orderService(){
        if(environment.getActiveProfiles()[0] == "dev"){
            return new Order;
        }else {
            return new ProdOrderServiceImpl(itemRepository, orderRepository);
        }
    }*/

}


@Order(1)
class HttpClientRestInterceptor implements ClientHttpRequestInterceptor {

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        request.getHeaders().add("Authorization", UUID.randomUUID().toString());
        System.out.println("Came inside the request inteceptor .");
        ClientHttpResponse response = execution.execute(request, body);
        // Add response headers
        response.getHeaders().add("Allow-access-origin", "*");
        return response;
    }
}
