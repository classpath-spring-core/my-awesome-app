package com.classpath.demoapp.config;

import com.classpath.demoapp.security.DomainUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
public class AppSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private DomainUserDetailsService userDetailsService;

    //handle the authentication
    @Override
    protected void configure(AuthenticationManagerBuilder authBuilder) throws Exception {
//        authBuilder
//                .inMemoryAuthentication()
//                .withUser("pradeep")
//                .password(passwordEncoder().encode("welcome"))
//                .roles("USER")
//                .and()
//                .withUser("ramesh")
//                .password(passwordEncoder().encode("welcome"))
//                .roles("USER", "ADMIN");

        authBuilder
                .userDetailsService(this.userDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }


    //handle the authorization
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.cors().disable();
        httpSecurity.csrf().disable();
        httpSecurity.headers().frameOptions().disable();
        httpSecurity
                .authorizeRequests()
                .antMatchers(GET,"/api/v1/orders/**")
                .hasAnyRole("USER", "ADMIN")
                .antMatchers(POST, "/api/v1/orders/**")
                .hasRole("ADMIN")
                .antMatchers("/h2-console/**")
                .permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .and()
                .httpBasic();

        httpSecurity.sessionManagement().sessionCreationPolicy(STATELESS);

    }
}