package com.classpath.demoapp.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BCryptPasswordEncoderUtil {

    public static void main(String[] args) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String chpherText1 = passwordEncoder.encode("welcome");
        String chpherText2 = passwordEncoder.encode("welcome");
        String chpherText3 = passwordEncoder.encode("welcome");

        System.out.println(chpherText1);
        System.out.println(chpherText2);
        System.out.println(chpherText3);

        System.out.println(passwordEncoder.matches("welcome", chpherText1));
        System.out.println(passwordEncoder.matches("welcome", chpherText1));
        System.out.println(passwordEncoder.matches("welcome", chpherText1));
    }

}