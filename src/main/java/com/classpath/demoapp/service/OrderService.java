package com.classpath.demoapp.service;

import com.classpath.demoapp.exception.ItemNotFoundException;
import com.classpath.demoapp.model.Item;
import com.classpath.demoapp.model.Order;

import java.util.Set;

public interface OrderService {

    Item saveItem(Item item);

    Set<Item> fetchAll();


    Item fetchItemById(long itemId) throws ItemNotFoundException;

    Item updateItem(long itemId, Item item) throws ItemNotFoundException;

    void deleteItemById(long itemId);

    Order placeOrder(Order order);

    Set<Order> fetchAllOrders();
}