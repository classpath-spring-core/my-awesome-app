package com.classpath.demoapp.service;

import com.classpath.demoapp.exception.ItemNotFoundException;
import com.classpath.demoapp.model.Item;
import com.classpath.demoapp.model.Order;
import com.classpath.demoapp.model.OrderLineItem;
import com.classpath.demoapp.repository.ItemRepository;
import com.classpath.demoapp.repository.OrderRepository;
import io.swagger.annotations.Authorization;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.annotation.security.RolesAllowed;
import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

@Service
@AllArgsConstructor
@Transactional
public class OrderServiceImpl implements OrderService {

    private ItemRepository itemRepository;

    private OrderRepository orderRepository;

    private static ItemNotFoundException itemNotFound() {
        return new ItemNotFoundException("Item not Found");
    }

    @Override
    public Item saveItem(Item item) {
        return this.itemRepository.save(item);
    }

    @Override
    public Set<Item> fetchAll() {
        return new HashSet<>(this.itemRepository.findAll());
    }

    @Override
    public Item fetchItemById(long itemId) throws ItemNotFoundException {
        return this.itemRepository.findById(itemId).orElseThrow(OrderServiceImpl::itemNotFound);
    }

    @Override
    public Item updateItem(long itemId, Item item) throws ItemNotFoundException {
        return this.itemRepository.findById(itemId).map(dbItem -> {
          dbItem.setName(item.getName());
          dbItem.setPrice(item.getPrice());
          return this.itemRepository.save(dbItem);
        }).orElseThrow(() -> new ItemNotFoundException("Item not found"));
    }

    @Override
    public void deleteItemById(long itemId) {
        this.itemRepository.deleteById(itemId);
    }

    @Override
    public Order placeOrder(Order order) {
        return this.orderRepository.save(order);
    }

    @Override
    public Set<Order> fetchAllOrders() {
        return new HashSet<>(this.orderRepository.findAll());
    }
}