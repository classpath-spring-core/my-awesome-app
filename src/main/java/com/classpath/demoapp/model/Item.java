package com.classpath.demoapp.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;


@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "items")
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "item_name", unique = true, nullable = false)
    @NotEmpty(message = "name cannot be empty")
    private String name;

    @Column(name = "item_price")
    @Max(25000)
    private double price;
}