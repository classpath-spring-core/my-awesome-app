package com.classpath.demoapp.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "orders")
@Setter
@Getter
@EqualsAndHashCode (of = "orderId")
@ToString
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long orderId;

    private int qty;

    private double price;

    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<OrderLineItem> lineItems = new HashSet<>();

    //scaffolding code
    public void addOrderItem(OrderLineItem orderLineItem){
        orderLineItem.setOrder(this);
    }
}