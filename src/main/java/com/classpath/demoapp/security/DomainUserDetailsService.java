package com.classpath.demoapp.security;

import com.classpath.demoapp.model.User;
import com.classpath.demoapp.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class DomainUserDetailsService implements UserDetailsService {

    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.userRepository
                .findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("Invalid user"));
        UserDetails domainUserDetails = new DomainUserDetails(user);
        return domainUserDetails;
    }
}