package com.classpath.demoapp.security;

import com.classpath.demoapp.model.Role;
import com.classpath.demoapp.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

import static java.util.stream.Collectors.toList;

public class DomainUserDetails implements  UserDetails{

    private final User user;

    public DomainUserDetails(User user) {
        this.user = user;
    }

    private static SimpleGrantedAuthority mapRoleToAuthority(Role role) {
        return new SimpleGrantedAuthority(role.getRoleName());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.user
                    .getRoles()
                    .stream()
                    .map(DomainUserDetails::mapRoleToAuthority)
                    .collect(toList());
    }

    @Override
    public String getPassword() {
        return this.user.getPassword();
    }

    @Override
    public String getUsername() {
        return this.user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}